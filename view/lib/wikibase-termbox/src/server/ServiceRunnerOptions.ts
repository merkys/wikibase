export default interface ServiceRunnerOptions {
	config: {
		WIKIBASE_REPO?: string,
		SSR_PORT?: string,
	};
}
