export default interface User {
	primaryLanguage: string;
	secondaryLanguages: string[];
}
