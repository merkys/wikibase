export const MEDIAWIKI_INDEX_SCRIPT = 'index.php';
export const MEDIAWIKI_API_SCRIPT = 'api.php';
export const GLOBAL_REQUEST_PARAMS = {
	format: 'json',
};
